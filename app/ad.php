<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Ad extends Model
{
    use SpatialTrait;
    protected $fillable = [
      'address',
      'type',
      'amount',
      'size',
      'room',
      'apt_floor',
      'floor',
      'bed',
      'bath',
      'year_built',
      'equipement',
      'heat',
      'dpe-energy',
      'dpe-emission',
      'published'
    ];
    protected $spatialFields = ['location'];

}
