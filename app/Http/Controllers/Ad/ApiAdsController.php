<?php

namespace App\Http\Controllers\Ad;

use App\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\AdCollection;
use App\Http\Resources\AdResource;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;

class ApiAdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $ads = ;
      return new AdCollection(AdResource::collection(Ad::where('published', 1)->get()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'address' => 'bail|required',
          'lng' => 'required',
          'lat' => 'required',
          'type' => 'required',
          'amount' => 'required',
          'size' => 'required',
          'room' => 'required'
      ]);
      $ad = Ad::create([
        'address'   => request('address'),
        'location'  => new Point(request(lat), request(lng)),
        'type'      => request('type'),
        'amount'    => request('amount'),
        'size'      => request('size'),
        'room'      => request('room'),
        'published' => 1
      ]);

      return response()->json([
          'post'    => $ad,
          'message' => 'Success'
      ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
      return new AdResource($ad);
    }

    public function search(Request $request)
    {

      $polygon = $request->toArray();
      $arrayOfPoints = [];
            foreach( $polygon['coords'] as $point ){

          $arrayOfPoints[] = new Point($point[0], $point[1]);
      }
      $lineString = new LineString($arrayOfPoints);
      $polygon = new Polygon([$lineString]);
      $ads = new Ad;
      return response(new AdCollection(AdResource::collection($ads->within('location', $polygon)->where('published', 1)->get())), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        //
    }
}
