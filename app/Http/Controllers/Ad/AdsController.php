<?php

namespace App\Http\Controllers\Ad;

use App\Ad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
class AdsController extends Controller
{
      use SpatialTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::all()->where('published',1);
        // dd($ads);
        return view('ads.list', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->validate([
          'address' => 'bail|required',
          'lng' => 'required',
          'lat' => 'required',
          'type' => 'required',
          'amount' => 'required',
          'size' => 'required',
          'room' => 'required'
      ]);
      // dd($request->input());

      $ad = new Ad();
      $ad->address = request('address');
      $ad->location = new Point(request('lat'),request('lng'));
      // dd(request('lat'));
      $ad->type = request('type');
      $ad->amount = request('amount');
      $ad->size = request('size');
      $ad->room = request('room');
      $ad->apt_floor = request('apt_floor');
      $ad->floor = request('floor');
      $ad->bed = request('bed');
      $ad->bath = request('bath');
      $ad->year_built = request('year_built');
      $ad->equipement = json_encode(request('equipement'));
      $ad->heat = request('heat');
      $ad->dpe_energy = request('dpe_energy');
      $ad->dpe_emission = request('dpe_emission');
      $ad->published = 1;
      // dd($ad);
      $ad->save();
      // $ad = Ad::create([
      //   'address'     => request('address'),
      //   'location'    => new Point(request('lat'), request('lng')),
      //   'type'        => request('type'),
      //   'amount'      => request('amount'),
      //   'size'        => request('size'),
      //   'room'        => request('room'),
      //   'apt_floor'   => request('apt_floor'),
      //   'floor'       => request('floor'),
      //   'bed'         => request('bed'),
      //   'bath'        => request('bath'),
      //   'year_built'  => request('year_built'),
      //   'equipement'  => request('equipement'),
      //   'heat'        => request('heat'),
      //   'dpe-energy'  => request('dpe-energy'),
      //   'dpe-emission'=> request('dpe-emission'),
      //   'published'   => 1
      // ]);
      return redirect()->route('ads.create')
              ->with('status','Ad created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(ad $ad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(ad $ad)
    {
        //
    }
}
