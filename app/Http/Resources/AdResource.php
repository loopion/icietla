<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AdResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'address' => $this->address,
          'coords' => array($this->location->getLat(),$this->location->getLng()),
          'amount' => $this->amount,
          'type' => ($this->type==1?"Maison":"Appartement"),
          'size' => $this->size,
          'room' => $this->room,
          'apt_floor' => $this->apt_floor,
          'floor' => $this->floor,
          'bed' => $this->bed,
          'bath' => $this->bath,
          'year_built' => $this->year_built,
          'equipement' => json_decode($this->equipement),
          'heat' => $this->heat,
          'dpe_energy' => $this->dpe_energy,
          'dpe_emission' => $this->dpe_emission,
          'published' => 1,
          'created_at' => $this->created_at->toDateTimeString(),
          'updated_at' => $this->updated_at->toDateTimeString(),
      ];
    }
}
