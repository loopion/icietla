## About IciEtLa

The idea is to replicate Redfin in a simplier way at the beginning.
Main highlighted features for France market:
* No real location hidden
* Show on a map exact goods locations with price tags
* Make a search by drawing areas of lookup

## Installing

Best way is to install [Homestead](https://laravel.com/docs/5.5/homestead).

Once installed

```
composer update
edit your .env file by copying the .env.example file
artisan key:generate
artisan migrate
artisan db:seed
npm install
npm run dev
```

## Access API

All routes are accessible through `<your-local-install>/api/v1` URI.

Example:

```
http://icietla.app/api/v1/ads
```