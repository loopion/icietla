<?php

use Faker\Generator as Faker;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Schema\Blueprint;
use Grimzy\LaravelMysqlSpatial\Types\Point;


$factory->define(App\Ad::class, function (Faker $faker) {
  $filename = base_path('database/seeds/idf-location.csv');
  $lines = file($filename);
  $nbLines = count($lines);
  foreach ($lines as $key => $value) {
    $lines[$key] = explode(',',$value);
  }

  $dpe_level = array ('a','b','c','d','e','f');
  $equipement = array();
  for ($i=0; $i < 12; $i++) {
    $equipement []= $i;
  }
    return [
        'address' => $faker->address,
        // 'location' => new Point($faker->latitude,$faker->longitude),
        'location' => new Point(trim($lines[$faker->numberBetween(1,$nbLines)][0]), trim($lines[$faker->numberBetween(1,$nbLines)][1])),
        'amount' => $faker->randomFloat(2,600000,2000000),
        'type' => $faker->randomDigitNotNull,
        'size' => $faker->randomDigitNotNull,
        'room' => $faker->randomDigitNotNull,
        'apt_floor' => $faker->randomDigitNotNull,
        'floor' => $faker->randomDigitNotNull,
        'bed' => $faker->randomDigitNotNull,
        'bath' => $faker->randomDigitNotNull,
        'year_built' => $faker->year,
        'equipement' => json_encode($faker->randomElements($equipement,5)),
        'heat' => $faker->numberBetween(1,6),
        'dpe_energy' => $faker->randomElement($dpe_level),
        'dpe_emission' => $faker->randomElement($dpe_level),
        'published' => $faker->boolean(90)
    ];
});
