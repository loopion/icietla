<?php

use Illuminate\Database\Seeder;
class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Ad::class, 100)->create();
      // factory(App\Ad::class, 50)->create()->each(function ($u) {
      //   DB::table('ads')
      //   ->where('id', $u->id)
      //   ->update(
      //     ['location' => DB::raw('ST_GeomFromText(\'POINT('.$faker->latitude.' '.$faker->longitude.')\')')]
      //   );
      // });
    }
}
