<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->point('location');
            // $table->float('lat',10,6);
            // $table->float('lng',10,6);
            $table->unsignedDecimal('amount', 10, 2);
            $table->unsignedTinyInteger('type');
            $table->unsignedInteger('size');
            $table->unsignedInteger('room');
            $table->unsignedTinyInteger('apt_floor');
            $table->unsignedTinyInteger('floor');
            $table->unsignedTinyInteger('bed');
            $table->unsignedTinyInteger('bath');
            $table->year('year_built');
            $table->string('equipement');
            $table->unsignedTinyInteger('heat');
            $table->char('dpe_energy',1);
            $table->char('dpe_emission',1);
            $table->boolean('published');
            $table->timestamps();
            // $table->index(['lng', 'lat']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
