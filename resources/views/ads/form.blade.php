<autocomplete-component></autocomplete-component>
<div class="form-group">
  <label for="amount" class="col-sm-3 control-label">Prix du bien</label>
  <div class="col-sm-5">
    <div class="input-group">
      <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount" maxlength="7">
      <div class="input-group-addon">€</div>
    </div>
  </div>
</div>

<div class="form-group">
  <label for="type" class="col-sm-3 control-label">Type</label>
  <div class="col-sm-3">
    <select class="form-control" name="type">
      <option value="1">Maison</option>
      <option value="2">Appartement</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label for="size" class="col-sm-3 control-label">Superficie</label>
  <div class="col-sm-2">
    <input type="text" class="form-control" id="size" name="size" placeholder="Superficie" maxlength="6">
  </div>
</div>
<div class="form-group">
  <label for="room" class="col-sm-3 control-label">Pièces</label>
  <div class="col-sm-2">
    <input type="text" class="form-control" id="room" name="room" placeholder="Pièces" maxlength="2">
  </div>
</div>
<div class="form-group">
  <label for="bed" class="col-sm-3 control-label">Chambres</label>
  <div class="col-sm-2">
    <input type="text" class="form-control" id="bed" name="bed" placeholder="Chambres" maxlength="2">
  </div>
</div>
<div class="form-group">
  <label for="bath" class="col-sm-3 control-label">Salle de bains</label>
  <div class="col-sm-9">
    <label class="radio-inline">
      <input type="radio" name="bath" id="bath0" value="0"> 0
    </label>
    <label class="radio-inline">
      <input type="radio" name="bath" id="bath1" value="1"> 1
    </label>
    <label class="radio-inline">
      <input type="radio" name="bath" id="bath2" value="2"> 2
    </label>
    <label class="radio-inline">
      <input type="radio" name="bath" id="bath2" value="3"> 3
    </label>
    <label class="radio-inline">
      <input type="radio" name="bath" id="bath2" value="4"> 4
    </label>
    <label class="radio-inline">
      <input type="radio" name="bath" id="bath2" value="5"> 5
    </label>
    <label class="radio-inline">
      <input type="radio" name="bath" id="bath2" value="9"> Plus
    </label>
  </div>
</div>
<div class="form-group">
  <label for="year_built" class="col-sm-3 control-label">Année de construction</label>
  <div class="col-sm-3">
    <input type="text" class="form-control" id="year_built" name="year_built" placeholder="Année de construction" maxlength="4">
  </div>
</div>
<div class="form-group">
  <label for="apt_floor" class="col-sm-3 control-label">Quel étage ce situe le bien</label>
  <div class="col-sm-3">
    <input type="text" class="form-control" id="apt_floor" name="apt_floor" placeholder="Etage" maxlength="3">
  </div>
</div>
<div class="form-group">
  <label for="floor" class="col-sm-3 control-label">Combien d'étage(s)</label>
  <div class="col-sm-1">
    <input type="text" class="form-control" id="floor" name="floor" placeholder="Nombre d'étage(s)" value="1" maxlength="1">
  </div>
  <span id="helpFloor" class="help-block">Il y a forcément un 1 étage. Ne pas considérer le parking ou la cave comme un étage.</span>
</div>

<div class="form-group">
  <label for="equipement" class="col-sm-3 control-label">Equipements</label>
  <div class="col-sm-9">
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement0" value="0"> Piscine
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement1" value="1"> Balcon
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement2" value="2"> Terrasse
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement3" value="3"> Cave
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement4" value="4"> Parking
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement5" value="5"> Cheminé
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement6" value="6"> Digicode
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement7" value="7"> Interphone
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement8" value="8"> Jardin
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement9" value="9"> Ascenceur
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement10" value="10"> Gardien
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement11" value="11"> Toilettes séparés
    </label>
    <label class="checkbox-inline">
      <input type="checkbox" name="equipement[]" id="equipement12" value="12"> Climatisation
    </label>
  </div>
  <div class="form-group">
    <label for="heat" class="col-sm-3 control-label">Chauffage</label>
    <div class="col-sm-9">
      <select class="form-control" name="heat">
        <option value="1">Individuel électrique</option>
        <option value="2">Individuel gaz</option>
        <option value="3">Individuel fioul</option>
        <option value="4">Collectif électrique</option>
        <option value="5">Collectif gaz</option>
        <option value="6">Collectif fioul</option>
        <option value="7">Autre</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="dpe_energy" class="col-sm-3 control-label">DPE : Bilan énergetique</label>
    <div class="col-sm-2">
      <select class="form-control" name="dpe_energy">
        <option value="A">A</option>
        <option value="B">B</option>
        <option value="C">C</option>
        <option value="D">D</option>
        <option value="E">E</option>
        <option value="F">F</option>
        <option value="G">G</option>
      </select>
    </div>
    <img src="https://www.ecologique-solidaire.gouv.fr/sites/default/files/styles/standard/public/%C3%89tiquette%20%C3%A9nergie%20-%20DPE.png?itok=CkVXz4EY" alt="DPE">
  </div>
  <div class="form-group">
    <label for="dpe_emission" class="col-sm-3 control-label">DPE : Bilan énergetique (GES)</label>
    <div class="col-sm-2">
      <select class="form-control" name="dpe_emission">
        <option value="A">A</option>
        <option value="B">B</option>
        <option value="C">C</option>
        <option value="D">D</option>
        <option value="E">E</option>
        <option value="F">F</option>
        <option value="G">G</option>
      </select>
    </div>
    <img src="https://www.ecologique-solidaire.gouv.fr/sites/default/files/styles/standard/public/%C3%89tiquette%20climat%20-%20DPE.png?itok=HzTRjDpk" alt="DPE">
  </div>


</div>

<!-- Buttons -->
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-primary">Ajouter le bien</button>
  </div>
</div>
@section('pagescript')
    <script type="text/javascript">
    var input = (document.getElementById('address-autocomplete'));
    var options = {
      types: ['geocode'],
      componentRestrictions: {country: 'fr'}
    };
    var places = new google.maps.places.Autocomplete(input, options);

    function onPlaceChanged() {
      var place = this.getPlace();

      // console.log(place);  // Uncomment this line to view the full object returned by Google API.

      for (var i in place.address_components) {
        var component = place.address_components[i];
        for (var j in component.types) {  // Some types are ["country", "political"]
          var type_element = document.getElementById(component.types[j]);
          if (type_element) {
            type_element.value = component.long_name;
          }
        }
      }
    }


    google.maps.event.addListener(places, 'place_changed', function () {
       var place = places.getPlace();
       var latitude = place.geometry.location.lat();
       var longitude = place.geometry.location.lng();
       console.log(latitude, longitude);
       document.getElementById('lat').value = latitude;
       document.getElementById('lng').value = longitude;
       // var address = place.formatted_address;
       // var latitude = place.geometry.location.A;
       // var longitude = place.geometry.location.F;
       // var mesg = "Address: " + address;
       // mesg += "\nLatitude: " + latitude;
       // mesg += "\nLongitude: " + longitude;
       // alert(mesg);
     });
    // autocomplete = new google.maps.places.Autocomplete((document.getElementById('address-autocomplete')),{types: ['geocode']});
    </script>
@stop
