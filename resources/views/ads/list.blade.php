@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listing</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a class="btn btn-link" href="{{ route('ads.create') }}">
                        Ajouter une adresse
                    </a>
                    <table class="table-striped">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Address</th>
                          <th>Longitude</th>
                          <th>Latitude</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($ads as $ad)
                        <tr>
                          <td>{{ $ad->id }}</td>
                          <td>{{ $ad->address }}</td>
                          <td>{{ $ad->location->getLat() }}</td>
                          <td>{{ $ad->location->getLng() }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
