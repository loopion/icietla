<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
  'prefix' => '/v1',
  'namespace' => 'Ad',
  'as' => 'api.'], function() {
    Route::post('ads/search', 'ApiAdsController@search')->name('ads.search');
    Route::apiResource('ads', 'ApiAdsController');
  });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
